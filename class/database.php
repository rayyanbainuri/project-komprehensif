<?php

	class Database {
		
		private $conn;
		 
		// Method Menghubungkan ke Database
		public function connect() {
				$host = "localhost";		// Nama Host
				$user = "root";				// Username phpMyAdmin
				$pass = "";					// Password phpMyAdmin
				$db   = "db_paperalikda";	// Nama Database
			$this->conn = new mysqli($host,$user,$pass,$db);
			return $this->conn;
		}
		
		// Method Untuk Memutuskan Koneksi Dengan Database
		public function close(){
			return $this->conn->close();
		}

	}
	
?>