<?php
	
	include_once "database.php";
	
	class Barang {
	
		public $jenis_barang;
		public $komponen;
		public $ukuran;
		public $satuan;
		public $tgl_perolehan;
		public $asal_usul_perolehan;
		public $harga;
		public $keterangan;
		public $gambar;
		public $permasalahan_kondisi_bmd;
		public $kib;
		
		// Atribut Barang KIB A
		public $register_kib_a;
		public $alamat;
		public $tgl_sertifikat_tanah;
		public $no_sertifikat_tanah;
		public $status_tanah;
		public $penggunaan;

		// Atribut Barang KIB B
		public $register_kib_b;
		public $bahan;
		public $merk;
		public $type;
		public $tgl_bpkb;
		public $nomor_bpkb;
		public $no_chasis;
		public $no_mesin;
		public $no_polisi;

		// jenis
		public $id_jenis;
		//id_jenis
		//kib
	
		

		// Method untuk Menampilkan bulan Data Terbaru KIB A
		public function getBulan_Kib_A() {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT DISTINCT YEAR(tgl_perolehan)as tahun, MONTH(tgl_perolehan)as bulan FROM kib_a ORDER by YEAR(tgl_perolehan) DESC ";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}
		// Method untuk Menampilkan bulan Data Terbaru KIB B
		public function getBulan_Kib_B() {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT DISTINCT YEAR(tgl_perolehan)as tahun, MONTH(tgl_perolehan)as bulan FROM kib_b ORDER by YEAR(tgl_perolehan) DESC";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}
		

		// Method untuk Menampilkan Semua Data Terbaru KIB A
		public function getDataNew_Kib_A($bulan,$tahun) {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT * from kib_a where  YEAR(tgl_perolehan)= '{$tahun}' AND MONTH(tgl_perolehan)='{$bulan}' order by register_kib_a asc ";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}
		// Method untuk Menampilkan Semua Data Terbaru KIB B
		public function getDataNew_Kib_B($bulan,$tahun) {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT * from kib_b where YEAR(tgl_perolehan)= '{$tahun}' AND MONTH(tgl_perolehan)='{$bulan}' order by register_kib_b asc ";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}
		

		// Method untuk Menampilkan Semua Data Terbaru KIB A
		public function getDemo_Kib_A($bln,$tahun) {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT Sum(harga)as harga, count(id_jenis)as jumlah, id_jenis from kib_a where  YEAR(tgl_perolehan)= '{$tahun}' AND MONTH(tgl_perolehan)='{$bln}' GROUP by id_jenis";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}
		// Method untuk Menampilkan Semua Data Terbaru KIB B
		public function getDemo_Kib_B($bln,$tahun) {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT  Sum(harga)as harga, count(id_jenis)as jumlah ,id_jenis from kib_b where YEAR(tgl_perolehan)= '{$tahun}' AND MONTH(tgl_perolehan)='{$bln}' GROUP by id_jenis";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}


		// Method untuk Menampilkan Semua Data Terbaru KIB A
		public function getDataNew_Kib_A_IRE($cari) {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT * from kib_a where id_jenis ='{$cari}' order by register_kib_a asc ";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}
		// Method untuk Menampilkan Semua Data Terbaru KIB B
		public function getDataNew_Kib_B_IRE($cari) {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT * from kib_b  where id_jenis ='{$cari}'  order by register_kib_b asc ";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}



		// Method untuk Memfilter sesuai jenis barang
		public function getDataFil_Kib_A($id_jenis,$bulan,$tahun) {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT * from kib_a where id_jenis = '{$id_jenis}' AND YEAR(tgl_perolehan)= '{$tahun}' AND MONTH(tgl_perolehan)='{$bulan}'";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}

		public function getDataFil_Kib_B($id_jenis,$bulan,$tahun) {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT * from kib_b where id_jenis = '{$id_jenis}' AND YEAR(tgl_perolehan)= '{$tahun}' AND MONTH(tgl_perolehan)='{$bulan}'";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}

		public function getTahun() {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT YEAR(tgl_perolehan) as tgl FROM kib_a group by YEAR(tgl_perolehan) DESC";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}
		public function getTahun2() {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT YEAR(tgl_perolehan) as tgl FROM kib_b group by YEAR(tgl_perolehan) DESC";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}

		// Method untuk menampilkan data detail KIB A
		public function getDetailKib_A($register_kib_a) {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT * FROM kib_a where register_kib_a = '{$register_kib_a}'";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data->fetch_array();
		}
		// Method untuk menampilkan data detail KIB B
		public function getDetailKib_B($register_kib_b) {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT * FROM kib_b where register_kib_b = '{$register_kib_b}'";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data->fetch_array();
		}

		public function getDataChunz1() {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT kib_a.id_jenis, count(register_kib_a) as jumlah, jenis_barang FROM kib_a inner join jenis_barang on kib_a.id_jenis = jenis_barang.id_jenis group by kib_a.id_jenis";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}

		public function getDataChunz2() {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT kib_b.id_jenis, count(register_kib_b) as jumlah, jenis_barang FROM kib_b inner join jenis_barang on kib_b.id_jenis = jenis_barang.id_jenis group by kib_b.id_jenis";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}

		public function getChartz1($id_jenis) {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT count(permasalahan_kondisi_bmd)as jumlah, permasalahan_kondisi_bmd as namab FROM  kib_a WHERE id_jenis='{$id_jenis}' GROUP by permasalahan_kondisi_bmd";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}

		public function getChartz2($id_jenis) {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT count(permasalahan_kondisi_bmd)as jumlah, permasalahan_kondisi_bmd as namab FROM kib_b  WHERE id_jenis='{$id_jenis}' GROUP by permasalahan_kondisi_bmd";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}

		public function getData_Jumlah_Kib_A() {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT count(*) as jumlah from kib_a";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}

		public function getData_Jumlah_Kib_B() {
			$db = new Database();
			$dbConnect = $db->connect();
			$sql = "SELECT count(*) as jumlah from kib_b";
			$data = $dbConnect->query($sql);
			$dbConnect = $db->close();
			return $data;
		}
	
/// ------------------------------------------------------------------------------------------------------	
	
		// Method Menambahkan Data Barang KIB A
		public function create_kib_a() {
		$db = new Database();
			// Membuka Koneksi ke Database
			$dbConnect = $db->connect();
			// Query Untuk Menyimpan Data
			$sql = 	"INSERT INTO kib_a
					(
						register_kib_a,
						id_jenis,
						komponen,
						ukuran,
						satuan,
						tgl_perolehan,
						alamat,
						tgl_sertifikat_tanah,
						no_sertifikat_tanah,
						status_tanah,
						penggunaan,
						asal_usul_perolehan,
						harga,
						keterangan,
						gambar,
						permasalahan_kondisi_bmd
					)
			VALUES
					(
						'{$this->register_kib_a}',
						'{$this->id_jenis}',
						'{$this->komponen}',
						'{$this->ukuran}',
						'{$this->satuan}',
						'{$this->tgl_perolehan}',
						'{$this->alamat}',
						'{$this->tgl_sertifikat_tanah}',
						'{$this->no_sertifikat_tanah}',
						'{$this->status_tanah}',
						'{$this->penggunaan}',
						'{$this->asal_usul_perolehan}',
						'{$this->harga}',
						'{$this->keterangan}',
						'{$this->gambar}',
						'{$this->permasalahan_kondisi_bmd}'
					);";
			// Eksekusi Query Diatas
			$data = $dbConnect->query($sql);
			// Menampung Error Query Simpan Data
			$error = $dbConnect->error;
			// Menutup Koneksi
			$dbConnect = $db->close();
			// Mengembalikan Nilai Error
			return $error;
		}

		// Method Update Barang KIB A
		public function update_kib_a() {
			$db = new Database();
			// Membuka Koneksi
			$dbConnect = $db->connect();
			// Query Menyimpan Data
			$data = mysqli_query($dbConnect ,"
					UPDATE kib_a SET
						id_jenis='{$this->id_jenis}',
						komponen='{$this->komponen}',
						ukuran='{$this->ukuran}',
						satuan='{$this->satuan}',
						tgl_perolehan='{$this->tgl_perolehan}',
						alamat='{$this->alamat}',
						tgl_sertifikat_tanah='{$this->tgl_sertifikat_tanah}',
						no_sertifikat_tanah='{$this->no_sertifikat_tanah}',
						status_tanah='{$this->status_tanah}',
						penggunaan='{$this->penggunaan}',
						asal_usul_perolehan='{$this->asal_usul_perolehan}',
						harga='{$this->harga}',
						keterangan='{$this->keterangan}',
						gambar='{$this->gambar}',
						permasalahan_kondisi_bmd='{$this->permasalahan_kondisi_bmd}'
					WHERE
						register_kib_a='{$this->register_kib_a}'
					");
			// Eksekusi Query Diatas
			//$data = $dbConnect—>query($sql); (Tidak bisa dipakai karena error)
			// Menampung Error Query Simpan Data
			$error = $dbConnect->error;
			// Menutup Koneksi
			$dbConnect = $db->close();
			// Mengembalikan Nilai Error
			return $error;
		}

		// Menghapus Data Barang KIB A
		public function delete_kib_a() {
			$db = new Database();
			// Membuka Koneksi
			$dbConnect = $db->connect();
			
			// Query Menyimpan Data
			$data = mysqli_query($dbConnect,
					"DELETE FROM kib_a
					WHERE register_kib_a = '{$this->register_kib_a}'");
			// Eksekusi Query Diatas
			// Menampung Error Query Simpan Data
			$error = $dbConnect->error;
			// Menutup Koneksi
			$dbConnect = $db->close();
			// Mengembalikan Nilai Error
			return $error;
		}


/// -----------------------------------------------------------------------------------------------------
		
		// Method Menambahkan Data Barang KIB B
		public function create_kib_b() {
		$db = new Database();
			// Membuka Koneksi ke Database
			$dbConnect = $db->connect();
			// Query Untuk Menyimpan Data
			$sql = 	"INSERT INTO kib_b
					(
						register_kib_b,
						id_jenis,
						komponen,
						ukuran,
						satuan,
						tgl_perolehan,
						bahan,
						merk,
						type,
						tgl_bpkb,
						nomor_bpkb,
						no_chasis,
						no_mesin,
						no_polisi,
						asal_usul_perolehan,
						harga,
						keterangan,
						gambar,
						permasalahan_kondisi_bmd
					)
			VALUES
					(
						'{$this->register_kib_b}',
						'{$this->id_jenis}',
						'{$this->komponen}',
						'{$this->ukuran}',
						'{$this->satuan}',
						'{$this->tgl_perolehan}',
						'{$this->bahan}',
						'{$this->merk}',
						'{$this->type}',
						'{$this->tgl_bpkb}',
						'{$this->nomor_bpkb}',
						'{$this->no_chasis}',
						'{$this->no_mesin}',
						'{$this->no_polisi}',
						'{$this->asal_usul_perolehan}',
						'{$this->harga}',
						'{$this->keterangan}',
						'{$this->gambar}',
						'{$this->permasalahan_kondisi_bmd}'
					);";
			// Eksekusi Query Diatas
			$data = $dbConnect->query($sql);
			// Menampung Error Query Simpan Data
			$error = $dbConnect->error;
			// Menutup Koneksi
			$dbConnect = $db->close();
			// Mengembalikan Nilai Error
			return $error;
		}

		// Method Update Barang KIB B
		public function update_kib_b() {
			$db = new Database();
			// Membuka Koneksi
			$dbConnect = $db->connect();
			// Query Menyimpan Data
			$data = mysqli_query($dbConnect ,"
					UPDATE kib_b SET
						id_jenis='{$this->id_jenis}',
						komponen='{$this->komponen}',
						ukuran='{$this->ukuran}',
						satuan='{$this->satuan}',
						tgl_perolehan='{$this->tgl_perolehan}',
						bahan='{$this->bahan}',
						merk='{$this->merk}',
						type='{$this->type}',
						tgl_bpkb='{$this->tgl_bpkb}',
						nomor_bpkb='{$this->nomor_bpkb}',
						no_chasis='{$this->no_chasis}',
						no_mesin='{$this->no_mesin}',
						no_polisi='{$this->no_polisi}',
						asal_usul_perolehan='{$this->asal_usul_perolehan}',
						harga='{$this->harga}',
						keterangan='{$this->keterangan}',
						gambar='{$this->gambar}',
						permasalahan_kondisi_bmd='{$this->permasalahan_kondisi_bmd}'
			WHERE
						register_kib_b='{$this->register_kib_b}'
					");
			// Eksekusi Query Diatas
			//$data = $dbConnect—>query($sql); (Tidak bisa dipakai karena error)
			// Menampung Error Query Simpan Data
			$error = $dbConnect->error;
			// Menutup Koneksi
			$dbConnect = $db->close();
			// Mengembalikan Nilai Error
			return $error;
		}
		
		// Menghapus Data Barang KIB B
		public function delete_kib_b() {
			$db = new Database();
			// Membuka Koneksi
			$dbConnect = $db->connect();
			
			// Query Menyimpan Data
			$data = mysqli_query($dbConnect,
					"DELETE FROM kib_b
					WHERE register_kib_b = '{$this->register_kib_b}'");
			// Eksekusi Query Diatas
			// Menampung Error Query Simpan Data
			$error = $dbConnect->error;
			// Menutup Koneksi
			$dbConnect = $db->close();
			
			// Mengembalikan Nilai Error
			return $error;
		}

	}

?>