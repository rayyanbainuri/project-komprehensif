<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">Pilihan Print</h4>
                  <p class="card-category">Print Laporan Data</p>
            </div>
        <div class="card-body">
        <br>
        <br>
        <div class="row">
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                    <a href="page/print/print_semua_windows.php"><i class="fa fa-print"></i></a>
                  </div>
                  <h3 class="card-category"></h3>
                  <h3 class="card-title"><strong>Print</strong></h3>
                  <p class="card-category">Semua Data</p>
                <br>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <a href="page/print/print_semua_download.php"><i class="fa fa-download"></i></a>
                  </div>
                  <h3 class="card-category"></h3>
                  <h3 class="card-title"><strong>Download</strong></h3>
                  <p class="card-category">Semua Data</p>
                <br>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
  
        