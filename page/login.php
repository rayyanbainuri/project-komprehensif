<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login | PAPERALIKDA Kecamatan Ciparay</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="../assets/img/icon.png">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../assets/login/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../assets/login/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../assets/login/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../assets/login/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../assets/login/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../assets/login/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../assets/login/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../assets/login/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../assets/login/util.css">
	<link rel="stylesheet" type="text/css" href="../assets/login/main.css">
<!--===============================================================================================-->
</head>
<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form action="../controller/cek_login.php" method="post" class="login100-form validate-form">
					<span class="login100-form-title p-b-26">
						<i class="zmdi zmdi-font"></i>
                        <img src="../assets/img/icon.png" width="130" height="110">
					</span>
					<span class="login100-form-title p-b-10">
						
					</span>
					<?php if (isset($_SESSION['error'])) {
						echo $_SESSION['error'];

						unset($_SESSION['error']);
						# code...
					}?>

					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="username">
						<span class="focus-input100" data-placeholder="Username"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<span class="btn-show-pass">
							<i class="fa fa-eye-slash"></i>
						</span>
						<input class="input100" type="password" name="password">
						<span class="focus-input100" data-placeholder="Password"></span>
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn">
								Login
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="../assets/login/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="../assets/login/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="../assets/login/popper.js"></script>
	<script src="../assets/login/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="../assets/login/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="../assets/login/moment.min.js"></script>
	<script src="../assets/login/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="../assets/login/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="../assets/login/main.js"></script>

</body>
</html>