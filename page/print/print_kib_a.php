<?php
if(isset($_POST['excel'])){
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan_KIB-A.xls");
}

?>
<?php
  include "../../class/jenis_barang.php";
  $jens = new JenisBarang();
  include "../../class/barang.php";
  $barang = new Barang();
  $jenis= $_POST['id_jenis'];
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Data Inventaris KIB A</title>
</head>

<body onload="window.print()">
<table width="1278" height="474" >
  <tr>
    <td colspan="2"> <img src="../../assets/img/icon.png" width="87" height="76" /></td>
    <td colspan="4"><p align="center"><strong>PEMERINTAHAN KABUPATEN BANDUNG<br />
      KARTU INVENTARIS (KIB) A<br />
    TANAH</strong></p></td>
  </tr>
  <tr>
    <td width="4" height="118">&nbsp;</td>
    <td width="235">
      <strong>Provinsi<br>
        Kab./Kota<br>
        Bidang<br>
        Unit Organisasi<br>
      Sub Unit Organisasi
      </strong>
      <p><strong>
       </strong><br>
      </p>
    </td>
    <td colspan="4">
    : Jawa Barat<br>
    : Pemerintah Kabupaten Bandung<br>
    : Kecamatan<br>
    : Kecamatan Ciparay<br>
    : Kecamatan Ciparay    <p>
  	 <br>
    </td>
  </tr>
  <tr>
    <td height="156" colspan="6">
      <table width="1271" border="1" style="border-collapse: collapse;">
      <tr>
        <td width="29" rowspan="3" align="center"><div align="center"><strong>NO.</strong></div></td>
        <td width="120" rowspan="3" align="center"><div align="center"><strong>Jenis Barang / Nama Barang</strong></div></td>
        <td colspan="2" rowspan="2" align="center"><div align="center"><strong>N o m o r</strong></div></td>
        <td width="60" rowspan="3" align="center"><div align="center"><strong>Lunas (M2)</strong></div></td>
        <td width="86" rowspan="3" align="center"><div align="center"><strong>Tahun Pengadaaan</strong></div></td>
        <td width="91" rowspan="3" align="center"><div align="center"><strong>Letak/ Alamat</strong></div></td>
        <td colspan="3" align="center"><div align="center"><strong>Status Tanah</strong></div></td>
        <td width="94" rowspan="3" align="center"><div align="center"><strong>Penggunaan</strong></div></td>
        <td width="105" rowspan="3" align="center"><div align="center"><strong>Asal usul</strong></div></td>
        <td width="153" rowspan="3" align="center"><div align="center"><strong>Harga (Ribuan RP)</strong></div></td>
        <td width="70" rowspan="3" align="center"><div align="center"><strong>Keterangan</strong></div></td>
        </tr>
      <tr>
        <td width="68" rowspan="2" align="center"><div align="center"><strong>Hak</strong></div></td>
        <td colspan="2" align="center"><div align="center"><strong>Sertifikat</strong></div></td>
      </tr>
      <tr>

        <td width="64" align="center"><div align="center"><strong>Register</strong></div></td>
        <td width="73" align="center"><div align="center"><strong>Tanggal</strong></div></td>
        <td width="71" align="center"><div align="center"><strong>Nomor</strong></div></td>
      </tr>
      <tr>
        <td align="center"><div align="center">1</div></td>
        <td align="center"><div align="center">2</div></td>
        <td align="center"><div align="center">3</div></td>
        <td align="center"><div align="center">4</div></td>
        <td align="center"><div align="center">5</div></td>
        <td align="center"><div align="center">6</div></td>
        <td align="center"><div align="center">7</div></td>
        <td align="center"><div align="center">8</div></td>
        <td align="center"><div align="center">9</div></td>
        <td align="center"><div align="center">10</div></td>
        <td align="center"><div align="center">11</div></td>
        <td align="center"><div align="center">12</div></td>
        <td align="center"><div align="center">13</div></td>
        <td align="center"><div align="center">14</div></td>
        </tr>
      <tr>
        <?php $jumlah = 0;
        $jenis= $_POST['id_jenis'];
        $bulan= date('m',strtotime($_POST['bulan']));
        $tahun= date('Y',strtotime($_POST['bulan']));

        if($jenis=="All"){
        foreach ($barang->getDataNew_Kib_A($bulan,$tahun) as $no=>$data_kib_a) : ?>
        <td align="center">&nbsp;<?= $no+1 ?></td>
        <td align="center">&nbsp;<?php  $jn=$jens->getDetail($data_kib_a['id_jenis']);
                                      echo $jn['jenis_barang']; ?></td>
        <td align="center">&nbsp;<?= $data_kib_a['register_kib_a'] ?></td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;<?= $data_kib_a['ukuran'] ?></td>
        <td align="center">&nbsp;<?= date($data_kib_a['tgl_perolehan']) ?></td>
        <td align="center">&nbsp;<?= $data_kib_a['alamat'] ?></td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;<?= $data_kib_a['asal_usul_perolehan'] ?></td>
        <td align="center">&nbsp;Rp. <?= number_format($data_kib_a['harga']) ?></td>
        <td align="center">&nbsp;</td>
        </tr>
        <?php 
          $jumlah+=$data_kib_a['harga'];
      endforeach;
    } else {
      foreach ($barang->getDataFil_Kib_A($jenis,$bulan,$tahun) as $no=>$data_kib_a) : ?>
        <td align="center">&nbsp;<?= $no+1 ?></td>
        <td align="center">&nbsp;<?php  $jn=$jens->getDetail($data_kib_a['id_jenis']);
                                      echo $jn['jenis_barang']; ?></td>
        <td align="center">&nbsp;<?= $data_kib_a['register_kib_a'] ?></td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;<?= $data_kib_a['ukuran'] ?></td>
        <td align="center">&nbsp;<?= date($data_kib_a['tgl_perolehan']) ?></td>
        <td align="center">&nbsp;<?= $data_kib_a['alamat'] ?></td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;<?= $data_kib_a['asal_usul_perolehan'] ?></td>
        <td align="center">&nbsp;Rp. <?= number_format($data_kib_a['harga']) ?></td>
        <td align="center">&nbsp;</td>
        </tr>
        <?php 
          $jumlah+=$data_kib_a['harga'];
      endforeach; }?> 
      <tr>
        <td colspan="9" align="center"><p  align="center">&nbsp;</p></td>
        <td colspan="3" align="center">Jumlah Harga</td>
        <td align="center">&nbsp;Rp. <?= number_format($jumlah) ?></td>
        <td align="center">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td height="67">&nbsp;</td>
    <td colspan="3" align="center"><p align="center">&nbsp;</p>
      <p align="center">MENGETAHUI<br />CAMAT</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p><strong>YUSUP SUPRIATNA, S.Ag, M.Si,</strong></p>
      <p>NIP. 19620310 198312 1 003</p>
      <p>&nbsp;</p>
    <p>&nbsp;</p></td>
    <td width="373">&nbsp;</td>
    <td width="640" align="center"><p align="center">Soreang, <br />
    </p>
      <p>PENGURUS BARANG</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p><strong>KARDIYA, S.IP</strong></p>
    <p>NIP. 19700114 201001 1 001</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p></td>
  </tr>
</table>
</body>
</html>