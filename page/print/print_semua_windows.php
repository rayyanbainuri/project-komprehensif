<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Kertas Kerja Rekonsiliasi</title>
<link rel="icon" type="image/png" href="../../assets/img/icon.png">
</head>

<body onload="window.print()">
<table border="1" cellspacing="0" cellpadding="0">
  <col width="14" />
  <col width="41" />
  <col width="255" />
  <col width="123" />
  <col width="96" />
  <col width="85" />
  <col width="45" />
  <col width="73" />
  <col width="100" />
  <col width="108" />
  <col width="102" />
  <col width="103" />
  <col width="105" />
  <col width="99" />
  <col width="97" />
  <col width="111" span="2" />
  <col width="99" />
  <col width="101" />
  <col width="92" />
  <col width="93" />
  <col width="103" />
  <col width="129" />
  <col width="131" />
  <tr>
    <td><a name="RANGE!A1:X101" id="RANGE!A1:X101"></a></td>
    <td colspan="23"><div align="center">KERTAS KERJA    REKONSILIASI  (KKR)</div></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="23" rowspan="2"><div align="center">PERHITUNGAN    SALDO AKHIR NERACA BMD PADA PENGGUNA BMD</div></td>
  </tr>
  <tr>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="23"><div align="center">PER 30 SEPTEMBER    2017</div></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="23"><div align="center">NAMA PERANGKAT    DAERAH</div></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="23"><div align="center"></div></td>
  </tr>
  <tr>
    <td></td>
    <td rowspan="4"><div align="center">NO</div></td>
    <td rowspan="4"><div align="center">URAIAN</div></td>
    <td rowspan="4"><div align="center">SALDO AWAL PER 1 JANUARI 2017</div></td>
    <td colspan="6"><div align="center">REALISASI BELANJA MODAL</div></td>
    <td colspan="6"><div align="center">PENAMBAHAN BMD</div></td>
    <td colspan="6"><div align="center">PENGURANGAN BMD</div></td>
    <td rowspan="4"><div align="center">SALDO AKHIR PER 30 SEPTEMBER 2017</div></td>
    <td rowspan="4"><div align="center">KETERANGAN</div></td>
  </tr>
  <tr>
    <td></td>
    <td colspan="6"><div align="center">PERIODE : 01 JANUARI 2017 S.D 30 SEPTEMBER 2017</div></td>
    <td colspan="6"><div align="center">PERIODE : 01 JANUARI 2017 S.D 30 SEPTEMBER 2017</div></td>
    <td colspan="6"><div align="center">PERIODE : 01 JANUARI 2017 S.D 30 SEPTEMBER 2017</div></td>
  </tr>
  <tr>
    <td></td>
    <td rowspan="2"><div align="center">MERK</div></td>
    <td rowspan="2"><div align="center">TYPE</div></td>
    <td rowspan="2"><div align="center">VOL</div></td>
    <td rowspan="2"><div align="center">SATUAN</div></td>
    <td rowspan="2"><div align="center">HARGA SATUAN (Rp)</div></td>
    <td rowspan="2"><div align="center">JUMLAH (Rp.)</div></td>
    <td rowspan="2"><div align="center">ATRIBUSI/     KAPITALISASI</div></td>
    <td rowspan="2"><div align="center">HIBAH</div></td>
    <td rowspan="2"><div align="center">PENGALIHAN/ MUTASI</div></td>
    <td rowspan="2"><div align="center">REKLASIFIKASI</div></td>
    <td rowspan="2"><div align="center">KOREKSI</div></td>
    <td rowspan="2"><div align="center">TOTAL</div></td>
    <td rowspan="2"><div align="center">PENGHAPUSAN</div></td>
    <td rowspan="2"><div align="center">PENGALIHAN / MUTASI</div></td>
    <td rowspan="2"><div align="center">REKLASIFIKASI</div></td>
    <td rowspan="2"><div align="center">EXTRA COMTABLE</div></td>
    <td rowspan="2"><div align="center">KOREKSI</div></td>
    <td rowspan="2"><div align="center">TOTAL</div></td>
  </tr>
  <tr>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td><div align="center">1</div></td>
    <td><div align="center">2</div></td>
    <td><div align="center">3</div></td>
    <td><div align="center">4</div></td>
    <td><div align="center">5</div></td>
    <td><div align="center">6</div></td>
    <td><div align="center">7</div></td>
    <td><div align="center">8</div></td>
    <td><div align="center">9 (6X8)</div></td>
    <td><div align="center">10</div></td>
    <td><div align="center">12</div></td>
    <td><div align="center">13</div></td>
    <td><div align="center">14</div></td>
    <td><div align="center">15</div></td>
    <td><div align="center">16</div></td>
    <td><div align="center"></div></td>
    <td><div align="center">18</div></td>
    <td><div align="center">19</div></td>
    <td><div align="center">20</div></td>
    <td><div align="center">21</div></td>
    <td><div align="center">22</div></td>
    <td><div align="center">23</div></td>
    <td><div align="center">24</div></td>
  </tr>
  <tr>
    <td></td>
    <td>&nbsp;</td>
    <td><div align="justify"></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td colspan="2"><strong>Belanja Tanah</strong></td>
    <td><div align="right"><strong>857,000,000 </strong></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><strong>                857,000,000 </strong></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td>&nbsp;</td>
    <td><div align="justify"></div></td>
    <td><div align="right"></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td>&nbsp;</td>
    <td><div align="justify"></div></td>
    <td><div align="right"></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td bgcolor="#FFCC66">&nbsp;</td>
    <td bgcolor="#FFCC66"><div align="justify"><strong>Sub Jumlah</strong></div></td>
    <td bgcolor="#FFCC66"><div align="right"><strong>857,000,000 </strong></div></td>
    <td bgcolor="#FFCC66">&nbsp;</td>
    <td bgcolor="#FFCC66">&nbsp;</td>
    <td bgcolor="#FFCC66">&nbsp;</td>
    <td bgcolor="#FFCC66">&nbsp;</td>
    <td align="center" bgcolor="#FFCC66">&nbsp;</td>
    <td align="center" bgcolor="#FFCC66">&nbsp;</td>
    <td bgcolor="#FFCC66">&nbsp;</td>
    <td bgcolor="#FFCC66">&nbsp;</td>
    <td bgcolor="#FFCC66">&nbsp;</td>
    <td bgcolor="#FFCC66">&nbsp;</td>
    <td bgcolor="#FFCC66">&nbsp;</td>
    <td bgcolor="#FFCC66">&nbsp;</td>
    <td bgcolor="#FFCC66">&nbsp;</td>
    <td bgcolor="#FFCC66">&nbsp;</td>
    <td bgcolor="#FFCC66">&nbsp;</td>
    <td bgcolor="#FFCC66">&nbsp;</td>
    <td bgcolor="#FFCC66">&nbsp;</td>
    <td bgcolor="#FFCC66">&nbsp;</td>
    <td bgcolor="#FFCC66"><strong>                857,000,000 </strong></td>
    <td bgcolor="#FFCC66">&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td>&nbsp;</td>
    <td><div align="justify"></div></td>
    <td><div align="right"></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td colspan="2"><div align="justify"><strong>Belanja Peralatan dan Mesin</strong></div></td>
    <td><div align="right"><strong>1,696,324,515 </strong></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><strong>             1,696,324,515 </strong></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td><div align="center">1</div></td>
    <td><div align="justify">Filling    Cabinet</div></td>
    <td>&nbsp;</td>
    <td><div align="justify">VIP</div></td>
    <td>&nbsp;</td>
    <td align="right"><div align="center">6</div></td>
    <td><div align="center">Buah </div></td>
    <td align="center"><div align="center">1,694,000 </div></td>
    <td align="center"><div align="center">10,164,000 </div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center">10,164,000 </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td><div align="center">2</div></td>
    <td><div align="justify">Pelt Bed</div></td>
    <td>&nbsp;</td>
    <td><div align="justify">Lokal</div></td>
    <td>&nbsp;</td>
    <td align="right"><div align="center">2</div></td>
    <td><div align="center">Buah </div></td>
    <td align="center"><div align="center">1,235,300 </div></td>
    <td align="center"><div align="center">2,470,600 </div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center">2,470,600 </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td><div align="center">3</div></td>
    <td><div align="justify">Podium</div></td>
    <td>&nbsp;</td>
    <td><div align="justify">Lokal</div></td>
    <td>&nbsp;</td>
    <td align="right"><div align="center">1</div></td>
    <td><div align="center">Buah </div></td>
    <td align="center"><div align="center">4,988,500 </div></td>
    <td align="center"><div align="center">4,988,500 </div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center">4,988,500 </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td><div align="center">4</div></td>
    <td><div align="justify">Kursi Pejabat</div></td>
    <td>&nbsp;</td>
    <td><div align="justify">Brother</div></td>
    <td>&nbsp;</td>
    <td align="right"><div align="center">5</div></td>
    <td><div align="center">Buah </div></td>
    <td align="center"><div align="center">1,985,500 </div></td>
    <td align="center"><div align="center">9,927,500 </div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center">9,927,500 </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td><div align="center">5</div></td>
    <td><div align="justify">Kursi Lipat</div></td>
    <td>&nbsp;</td>
    <td><div align="justify">Chitose</div></td>
    <td>&nbsp;</td>
    <td align="right"><div align="center">75</div></td>
    <td><div align="center">Buah </div></td>
    <td align="center"><div align="center">398,200 </div></td>
    <td align="center"><div align="center">29,865,000 </div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">29,865,000 </td>
    <td align="right">        </td>
    <td align="right">29,865,000 </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td><div align="center">6</div></td>
    <td><div align="justify">Kursi Tunggu Tamu</div></td>
    <td>&nbsp;</td>
    <td><div align="justify">3 DPK</div></td>
    <td>&nbsp;</td>
    <td align="right"><div align="center">4</div></td>
    <td><div align="center">Buah </div></td>
    <td align="center"><div align="center">1,441,000 </div></td>
    <td align="center"><div align="center">5,764,000 </div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center">5,764,000 </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td><div align="center">7</div></td>
    <td><div align="justify">Televisi</div></td>
    <td>&nbsp;</td>
    <td><div align="justify">LG</div></td>
    <td>&nbsp;</td>
    <td align="right"><div align="center">1</div></td>
    <td><div align="center">Unit </div></td>
    <td align="center"><div align="center">4,488,000 </div></td>
    <td align="center"><div align="center">4,488,000 </div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center">4,488,000 </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td><div align="center">8</div></td>
    <td><div align="justify">Lemari Es</div></td>
    <td>&nbsp;</td>
    <td><div align="justify">AQUA</div></td>
    <td>&nbsp;</td>
    <td align="right"><div align="center">1</div></td>
    <td><div align="center">Unit </div></td>
    <td align="center"><div align="center">1,991,000 </div></td>
    <td align="center"><div align="center">1,991,000 </div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center">1,991,000 </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td><div align="center">9</div></td>
    <td><div align="justify">Laptop</div></td>
    <td>&nbsp;</td>
    <td><div align="justify">Asus</div></td>
    <td><div align="justify">Intel Core I3</div></td>
    <td align="right"><div align="center">3</div></td>
    <td><div align="center">Unit </div></td>
    <td align="center"><div align="center">6,990,500 </div></td>
    <td align="center"><div align="center">20,971,500 </div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center">20,971,500 </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td><div align="center">10</div></td>
    <td><div align="justify">Hardisk Eksternal</div></td>
    <td>&nbsp;</td>
    <td><div align="justify">WD</div></td>
    <td><div align="justify">Element</div></td>
    <td align="right"><div align="center">2</div></td>
    <td><div align="center">Unit </div></td>
    <td align="center"><div align="center">891,000 </div></td>
    <td align="center"><div align="center">1,782,000 </div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center">1,782,000 </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td><div align="center">11</div></td>
    <td><div align="justify">Printer Scanner</div></td>
    <td>&nbsp;</td>
    <td><div align="justify">Epson</div></td>
    <td><div align="justify">L 360</div></td>
    <td align="right"><div align="center">1</div></td>
    <td><div align="center">Unit </div></td>
    <td align="center"><div align="center">3,487,000 </div></td>
    <td align="center"><div align="center">3,487,000 </div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center">3,487,000 </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td><div align="center">12</div></td>
    <td><div align="justify">Microphone</div></td>
    <td>&nbsp;</td>
    <td><div align="justify">Rexon</div></td>
    <td><div align="justify"></div></td>
    <td align="right"><div align="center">2</div></td>
    <td><div align="center">Set </div></td>
    <td align="center"><div align="center">990,000 </div></td>
    <td align="center"><div align="center">1,980,000 </div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center">1,980,000 </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td><div align="center">13</div></td>
    <td><div align="justify">Camera</div></td>
    <td>&nbsp;</td>
    <td><div align="justify">Canon</div></td>
    <td><div align="justify">SX</div></td>
    <td align="right"><div align="center">1</div></td>
    <td><div align="center">Unit </div></td>
    <td align="center"><div align="center">7,491,000 </div></td>
    <td align="center"><div align="center">7,491,000 </div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center">7,491,000 </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td><div align="center">14</div></td>
    <td><div align="justify">Projector</div></td>
    <td>&nbsp;</td>
    <td><div align="justify">Ben Q</div></td>
    <td><div align="justify">M5</div></td>
    <td align="right"><div align="center">1</div></td>
    <td><div align="center">Unit </div></td>
    <td align="center"><div align="center">6,985,000 </div></td>
    <td align="center"><div align="center">6,985,000 </div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center">6,985,000 </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td><div align="center">15</div></td>
    <td><div align="justify">Layar Infocus</div></td>
    <td>&nbsp;</td>
    <td><div align="justify">Lokal</div></td>
    <td><div align="justify"></div></td>
    <td align="right"><div align="center">1</div></td>
    <td><div align="center">Unit </div></td>
    <td align="center"><div align="center">737,000 </div></td>
    <td align="center"><div align="center">737,000 </div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center">737,000 </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td><div align="center"></div></td>
    <td><div align="justify"></div></td>
    <td>&nbsp;</td>
    <td><div align="justify"></div></td>
    <td><div align="justify"></div></td>
    <td><div align="center"></div></td>
    <td><div align="center"></div></td>
    <td align="center"><div align="center"></div></td>
    <td align="center"><div align="center"></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center"></div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td bgcolor="#FFCC33"><div align="center"></div></td>
    <td bgcolor="#FFCC33"><div align="justify">Sub Jumlah</div></td>
    <td bgcolor="#FFCC33"><div align="right">1,696,324,515 </div></td>
    <td bgcolor="#FFCC33"><div align="justify"></div></td>
    <td bgcolor="#FFCC33"><div align="justify"></div></td>
    <td bgcolor="#FFCC33"><div align="center"></div></td>
    <td bgcolor="#FFCC33"><div align="center"></div></td>
    <td align="center" bgcolor="#FFCC33"><div align="center"></div></td>
    <td align="center" bgcolor="#FFCC33"><div align="center">113,092,100 </div></td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33"><div align="center">29,865,000 </div></td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33"><div align="center">29,865,000 </div></td>
    <td bgcolor="#FFCC33"><div align="center">1,779,551,615 </div></td>
    <td bgcolor="#FFCC33">&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td><div align="center"></div></td>
    <td><div align="justify"></div></td>
    <td><div align="right"></div></td>
    <td><div align="justify"></div></td>
    <td><div align="justify"></div></td>
    <td><div align="center"></div></td>
    <td><div align="center"></div></td>
    <td align="center"><div align="center"></div></td>
    <td align="center"><div align="center"></div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center"></div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td colspan="2"><div align="justify">Aset lain-lain /Rusak Berat</div></td>
    <td>                 
    <div align="right">37,414,400 </div></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><div align="center">37,414,400 </div></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td bgcolor="#FFCC33"><div align="justify"></div></td>
    <td bgcolor="#FFCC33"><div align="justify">Sub Jumlah</div></td>
    <td bgcolor="#FFCC33"><div align="right">37,414,400 </div></td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td align="center" bgcolor="#FFCC33">&nbsp;</td>
    <td align="center" bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33">&nbsp;</td>
    <td bgcolor="#FFCC33"><div align="center">37,414,400 </div></td>
    <td bgcolor="#FFCC33">&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td colspan="2" bgcolor="#009933"><div align="justify"><strong>JUMLAH ASET LAINNYA</strong></div></td>
    <td bgcolor="#009933"><div align="right">37,414,400 </div></td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td align="center" bgcolor="#009933">&nbsp;</td>
    <td align="center" bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933"><div align="center">37,414,400</div></td>
    <td bgcolor="#009933">&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td colspan="2" bgcolor="#009933"><div align="justify"><strong>JUMLAH ASET TETAP + ASET LAINNYA</strong></div></td>
    <td bgcolor="#009933"><div align="right">6,155,034,695 </div></td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td align="center" bgcolor="#009933">&nbsp;</td>
    <td align="center" bgcolor="#009933"><div align="center">332,369,100 </div></td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933"><div align="center">29,865,000 </div></td>
    <td bgcolor="#009933">&nbsp;</td>
    <td bgcolor="#009933"><div align="center">29,865,000 </div></td>
    <td bgcolor="#009933"><div align="center">6,457,538,795 </div></td>
    <td bgcolor="#009933">&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr> 
  </table>
</body>
</html>