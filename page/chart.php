<script>
// Create the chart
Highcharts.chart('chart1', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Persentase Fisik Barang KIB A Yang Tersedia'
  },
  subtitle: {
    text: ''
  },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total percent'
        }

    },
    legend: {
        enabled: false
    },
  plotOptions: {
    series: {
      dataLabels: {
        enabled: true,
        format: '{point.y:f}'
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> of total<br/>'
  },

  "series": [
    {
      "name": "Data",
      "colorByPoint": true,
      "data": [
      <?php foreach ($barang->getDataChunz1() as $key): ?>
        {
          "name": "<?= $key['jenis_barang']?>",
          "y": <?= $key['jumlah']?>,
          "drilldown": "<?= $key['jenis_barang']?>"
        },
        <?php endforeach ?> 
      ]
    }
  ],
  "drilldown": {
    "series": [
      <?php foreach ($barang->getDataChunz1() as $da): ?>
      {
        "name": "<?= $da['jenis_barang']?>",
        "id": "<?= $da['jenis_barang']?>",
        "data": [

      <?php foreach ($barang->getChartz1($da['id_jenis']) as $sa): ?>
          [
            "<?= $sa['namab']?>",
            <?= $sa['jumlah']?>
          ],
        <?php endforeach ?> 

        ]

      },
        <?php endforeach ?> 
      
      
    ]
  }
});



</script>

<script>
// Create the chart
Highcharts.chart('chart2', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Persentase Fisik Barang KIB B Yang Tersedia'
  },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total percent'
        }

    },
    legend: {
        enabled: false
    },
  subtitle: {
    text: ''
  },
  plotOptions: {
    series: {
      dataLabels: {
        enabled: true,
        format: '{point.y:f}'
      }
    }
  },

  tooltip: {
    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> of total<br/>'
  },

  "series": [
    {
      "name": "Data",
      "colorByPoint": true,
      "data": [
      <?php foreach ($barang->getDataChunz2() as $key): ?>
        {
          "name": "<?= $key['jenis_barang']?>",
          "y": <?= $key['jumlah']?>,
          "drilldown": "<?= $key['jenis_barang']?>"
        },
        <?php endforeach ?> 
      ]
    }
  ],
  "drilldown": {
    "series": [
      <?php foreach ($barang->getDataChunz2() as $da): ?>
      {
        "name": "<?= $da['jenis_barang']?>",
        "id": "<?= $da['jenis_barang']?>",
        "data": [

      <?php foreach ($barang->getChartz2($da['id_jenis']) as $sa): ?>
          [
            "<?= $sa['namab']?>",
            <?= $sa['jumlah']?>
          ],
        <?php endforeach ?> 

        ]

      },
        <?php endforeach ?> 
      
      
    ]
  }
});
</script>