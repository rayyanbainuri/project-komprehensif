<?php 
include_once "class/jenis_barang.php";
$jenis_barang = new JenisBarang();
?>
<div class="row">
	<div class="col-md-8">
		<div class="card">
			<div class="card-header card-header-primary">
				<h4 class="card-title"><strong>Form Input Data</strong></h4>
				<p class="card-category">KIB A (Tanah)</p>
			</div>
<!-- 			<form action="controller/kib_a/tambahBarang.php">
				<div class="card-body ">
					<label>Tambah Barang</label>
					<div class="row form-group ">
						<div class="col-sm-8">
							<input type="text" name="jenis_barang" class="form-control" required="">
						</div>
						<div class="col-sm-4">
							<button type="submit" class="btn btn-primary btn-sm">Tambah</button>
						</div>
					</div>
					<hr>
				</div>
			</form> -->
			<form action="controller/kib_a/create_kib_a.php" method="Post" enctype="multipart/form-data"> 
				<div class="card-body">
					<div class="row">
						<div class="col-sm-12 col-lg-12">
							<div class="form-group">
								<label for="exampleInput1" class="label">Jenis Barang</label>
								<select  class="form-control" name="id_jenis" required>
									<?php foreach ($jenis_barang->getData('a') as $val ): ?>
										<option value="<?php echo $val['id_jenis'] ?>"><?php echo $val['jenis_barang'] ?></option>
									<?php endforeach ?>
								</select>
								<span class="bmd-help"></span>
							</div>
							<div id="hidden1">
							<div class="form-group">
								<label for="exampleInput1" class="label">Komponen</label>
								<input type="text" class="form-control" name="komponen" required>
								<span class="bmd-help"></span>
							</div>
							<div class="form-group">
								<label for="exampleInput1" class="label">Ukuran</label>
								<input type="text" class="form-control" name="ukuran" required>
								<span class="bmd-help"></span>
							</div>
							<div class="form-group">
								<label for="exampleInput1" class="label">Satuan</label>
								<select  class="form-control" name="satuan" required>
									<option value="Hektar">ha</option>
									<option value="Kilometer">km<sup>2</sup></option>
									<option value="Meter">m<sup>2</sup></option>
								</select>
								<span class="bmd-help"></span>
							</div>
							<div class="form-group">
								<label for="exampleInput1" class="label">Tanggal Perolehan</label>
								<input type="date" class="form-control" name="tgl_perolehan" required>
								<span class="bmd-help"></span>
							</div>
							<div class="form-group">
								<label for="exampleInput1" class="label">Alamat</label>
								<input type="text" class="form-control" name="alamat">
								<span class="bmd-help"></span>
							</div>
							<div class="form-group">
								<label for="exampleInput1" class="label">Tanggal Sertifikat Tanah</label>
								<input type="date" class="form-control" name="tgl_sertifikat_tanah">
								<span class="bmd-help"></span>
							</div>
							<div class="form-group">
								<label for="exampleInput1" class="label">No Sertifikat Tanah</label>
								<input type="text" class="form-control" name="no_sertifikat_tanah">
								<span class="bmd-help"></span>
							</div>
							<div class="form-group">
								<label for="exampleInput1" class="label">Status Tanah</label>
								<select  class="form-control" name="status_tanah">
									<option value="Milik Kabupaten Bandung">Milik Kabupaten Bandung</option>
									<option value="Milik Negara">Milik Negara</option>
									<option value="Hak Pakai">Hak Pakai</option>
									<option value="Swadaya">Swadaya</option>
									<option value="Hak Guna Bangunan">Hak Guna Bangunan</option>
									<option value="Hak Pengelolaan">Hak Pengelolaan</option>
									<option value="Wakaf">Wakaf</option>
									<option value="Hak Lainnya">Hak Lainnya</option>
									<option value="Lain-lain">Lain-lain</option>
								</select>
								<span class="bmd-help"></span>
							</div>
							<div class="form-group">
								<label for="exampleInput1" class="label">Penggunaan</label>
								<input type="text" class="form-control" name="penggunaan">
								<span class="bmd-help"></span>
							</div>
							<div class="form-group">
								<label for="exampleInput1" class="label">Asal-usul Perolehan</label>
								<select  class="form-control" name="asal_usul_perolehan" required>
									<option value="Pembangunan">Pembangunan</option>
									<option value="Pembelian">Pembelian / APBD</option>
									<option value="Hibah">Hibah</option>
									<option value="Guna Usaha">Guna Usaha</option>
									<option value="Tukar Guling">Tukar Guling</option>
									<option value="Ruislag">Ruislag</option>
									<option value="BOP">BOP</option>
									<option value="Asal Milik Adat">Asal Milik Adat</option>
									<option value="Sekolah">Sekolah</option>
									<option value="Sumbangan">Sumbangan</option>
									<option value="Swadaya">Swadaya</option>
									<option value="Ex Kanwil">Ex Kanwil</option>
									<option value="Lain-lain">Lain-lain</option>
								</select>
								<span class="bmd-help"></span>
							</div>
							<div class="form-group">
								<label for="exampleInput1" class="label">Harga (Rp.)</label>
								<input type="number" class="form-control" name="harga" required>
								<span class="bmd-help"></span>
							</div>
							<div class="form-group">
								<label for="exampleInput1" class="label">Keterangan</label>
								<input type="text" class="form-control" name="keterangan">
								<span class="bmd-help"></span>
							</div>
							<div class="form-group">
								<label for="exampleInput1" class="label">Kondisi BMD</label>
								<select  class="form-control" name="permasalahan_kondisi_bmd" required>
									<option value="Baik">Baik</option>
									<option value="Rusak">Rusak</option>
									<option value="Rusak Berat">Rusak Berat</option>
								</select>
								<span class="bmd-help"></span>
							</div>
							<br>
							<center>
								<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp; Tambah Data</button>
							</center>
							<br>
						
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card">
				<div class="card-header card-header-primary">
					<h4 class="card-title"><strong>Gambar</strong></h4>
					<p class="card-category">KIB A (Tanah)</p>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-sm-6 col-lg-5">
							<img style="border-radius:1%" width='300' height='250' src='assets/img/no-image.png' id="output_image" />
							<input type="file" name="gambar" accept="image/x-png,image/gif,image/jpeg" onchange="preview_image(event)" class="form-control"  required>
							<br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
	<script type='text/javascript'>

		function preview_image(event)
		{
			var reader = new FileReader();
			reader.onload = function()
			{
				var output = document.getElementById('output_image');
				output.src = reader.result;
			}
			reader.readAsDataURL(event.target.files[0]);
		}</script>