<?php if(isset($_SESSION['message'])) : ?>
		<!-- Jika Terdapat Error Maka Munculkan Pesan Pada Session Yang Telah Dibuat -->
		<p>
			<div class="alert alert-danger" role="alert"> Gagal Menyimpan Data : <?= $_SESSION['message'] ?>
			</div>
		</p>
		<!-- Mengosongkan Session Message Agar Pesan Tidak Muncul Kembali -->
		<?php unset($_SESSION['message']); ?>
<?php endif; ?>

<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title"><strong>Detail Barang</strong></h4>
                <p class="card-category">KIB A (Tanah)</p>
            </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                      	<?php $kib_a = $barang->getDetailKib_A($_GET['register_kib_a']) ?>
							<td width="150px">
							Register</td>
							<td>:</td>
							<td><?= $kib_a['register_kib_a'] ?></td>
                      </thead>
                      <tbody>
                        <tr>
							<td>
							Jenis Barang</td>
							<td>:</td>
							<td><?php  $jn=$jenis_barang->getDetail($kib_a['id_jenis']);
                                      echo $jn['jenis_barang']; ?></td>
                        </tr>
                        <tr>
							<td>
							Komponen</td>
							<td>:</td>
							<td><?= $kib_a['komponen'] ?></td>
                        </tr>
                        <tr>
							<td>
							Ukuran</td>
							<td>:</td>
							<td><?= $kib_a['ukuran'] ?></td>
                        </tr>
						<tr>
							<td>
							Satuan</td>
							<td>:</td>
							<td><?= $kib_a['satuan'] ?></td>
                        </tr>
						<tr>
							<td>
							Tanggal Perolehan</td>
							<td>:</td>
							<td><?= date($kib_a['tgl_perolehan']) ?></td>
                        </tr>
						<tr>
							<td>
							Alamat</td>
							<td>:</td>
							<td><?= $kib_a['alamat'] ?></td>
                        </tr>
                        <tr>
							<td>
							Tanggal Sertifikat Tanah</td>
							<td>:</td>
							<td><?= date($kib_a['tgl_sertifikat_tanah']) ?></td>
                        </tr>
                        <tr>
							<td>
							No Sertifikat Tanah </td>
							<td>:</td>
							<td><?= $kib_a['no_sertifikat_tanah'] ?></td>
                        </tr>
                        <tr>
							<td>
							Status Tanah</td>
							<td>:</td>
							<td><?= $kib_a['status_tanah'] ?></td>
                        </tr>
						<tr>
							<td>
							Penggunaan</td>
							<td>:</td>
							<td><?= $kib_a['penggunaan'] ?></td>
                        </tr>
						<tr>
							<td>
							Asal-usul Perolehan</td>
							<td>:</td>
							<td><?= $kib_a['asal_usul_perolehan'] ?></td>
                        </tr>
						<tr>
							<td>
							Harga (Rp.)</td>
							<td>:</td>
							<td>Rp. <?= number_format($kib_a['harga']); ?></td>
                        </tr>
						<tr>
							<td>
							Keterangan</td>
							<td>:</td>
							<td><?= $kib_a['keterangan'] ?></td>
                        </tr>
						<tr>
							<td>
							Kondisi BMD</td>
							<td>:</td>
							<td><a class="btn btn-<?php if ($kib_a['permasalahan_kondisi_bmd']=='Baik'){echo "success";} else if ($kib_a['permasalahan_kondisi_bmd']=='Rusak'){echo "danger";} if ($kib_a['permasalahan_kondisi_bmd']=='Rusak Berat'){echo "warning";}?>" style="width:130px"><?= $kib_a['permasalahan_kondisi_bmd'] ?></a></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
			<div class="col-md-4">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Gambar</h4>
                  <p class="card-category">KIB A (Tanah)</p>
                </div>
                <div class="card-body">
                  <div class="row">
                   <div class="col-sm-6 col-lg-5">
				   <img style="border-radius:1%; max-width: 293px; max-height: 250px; min-height: 250px; min-width: 293px" width='293' height='250' src='images/<?= $kib_a['gambar']; ?>'/>
                <?php ?>
                 <br>
                  </div>
                </div>
              </div>
            </div>
           </div>
         </div>