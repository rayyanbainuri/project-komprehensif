<?php if(isset($_SESSION['message'])) : ?>
		<!-- Jika Terdapat Error maka akan memunculkan Pesan pada Session yang telah dibuat -->
		<p>
			<div class="alert alert-danger" role="alert"> Gagal Merubah Data : <?= $_SESSION['message'] ?>
			</div>
		</p>
		<!-- Mengosongkan Session Message agar Pesan tidak muncul kembali -->
		<?php unset($_SESSION['message']); ?>
<?php endif; ?>

<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title">Form Ubah Data</h4>
                <p class="card-category">KIB A (Tanah)</p>
            </div>
                <div class="card-body">
                  <div class="table-responsive">
                  	<form action="controller/kib_a/update_kib_a.php" method="post" enctype="multipart/form-data">
                    <table class="table">
                      <thead class=" text-primary">
                      	<input type="hidden" name="register_kib_a" value="<?= $_GET['register_kib_a'] ?>"/>
                      	<?php $kib_a = $barang->getDetailKib_A($_GET['register_kib_a']); ?>
							<td width="150px">
							Register</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="register_kib_a" value="<?= $kib_a['register_kib_a'] ?>" disabled></td>
                      </thead>
                      <tbody>
                        <tr>
							<td>
							Jenis Barang</td>
							<td>:</td>
							<td><select  class="form-control" name="id_jenis" disabled="">
									<?php foreach ($jenis_barang->getData('a') as $val ): ?>
										<option value="<?php echo $val['id_jenis'] ?>"><?php echo $val['jenis_barang'] ?></option>
									<?php endforeach ?>
								</select></td>
                        </tr>
                        <tr>
							<td>
							Komponen</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="komponen" value="<?= $kib_a['komponen'] ?>" required></td>
                        </tr>
                        <tr>
							<td>
							Ukuran</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="ukuran" value="<?= $kib_a['ukuran'] ?>" required></td>
                        </tr>
						<tr>
							<td>
							Satuan</td>
							<td>:</td>
							<td><select  class="form-control" name="satuan" required>
								<option value="Hektar" <?php if ($kib_a['satuan']=="Hektar") {
									echo "selected";
								}?> >ha</option>
								<option value="Kilometer" <?php if ($kib_a['satuan']=="Kilometer") {
									echo "selected";
								}?> >km<sup>2</sup></option>
								<option value="Meter" <?php if ($kib_a['satuan']=="Meter") {
									echo "selected";
								}?> >m<sup>2</sup></option>
								</select>
                        </tr>
						<tr>
							<td>
							Tanggal Perolehan</td>
							<td>:</td>
							<td><input type="date" class="form-control" name="tgl_perolehan" value="<?= $kib_a['tgl_perolehan'] ?>"></td>
                        </tr>
						<tr>
							<td>
							Alamat</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="alamat" value="<?= $kib_a['alamat'] ?>"></td>
                        </tr>
                        <tr>
							<td>
							Tanggal Sertifikat Tanah</td>
							<td>:</td>
							<td><input type="date" class="form-control" name="tgl_sertifikat_tanah" value="<?= $kib_a['tgl_sertifikat_tanah'] ?>"></td>
                        </tr>
                        <tr>
							<td>
							No Sertifikat Tanah </td>
							<td>:</td>
							<td><input type="text" class="form-control" name="no_sertifikat_tanah" value="<?= $kib_a['no_sertifikat_tanah'] ?>"></td>
                        </tr>
                        <tr>
							<td>
							Status Tanah</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="status_tanah" value="<?= $kib_a['status_tanah'] ?>"></a></td>
                        </tr>
						<tr>
							<td>
							Penggunaan</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="penggunaan" value="<?= $kib_a['penggunaan'] ?>"></td>
                        </tr>
						<tr>
							<td>
							Asal-usul Perolehan</td>
							<td>:</td>
							<td>
								<select  class="form-control" name="asal_usul_perolehan" required>
								<option value="Hibah" <?php if ($kib_a['asal_usul_perolehan']=="Hibah") {
									echo "selected";
								}?> >Hibah</option>
								<option value="Membangun Sendiri" <?php if ($kib_a['asal_usul_perolehan']=="Membangun Sendiri") {
									echo "selected";
								}?> >Membangun Sendiri</option>
								</select>
							</td>
                        </tr>
						<tr>
							<td>
							Harga (Rp.)</td>
							<td>:</td>
							<td>Rp. <input type="number" class="form-control" name="harga" value="<?= $kib_a['harga'] ?>"></td>
                        </tr>
						<tr>
							<td>
							Keterangan</td>
							<td>:</td>
							<td><input type="text" class="form-control" name="keterangan" value="<?= $kib_a['keterangan'] ?>"></td>
                        </tr>
						<tr>
							<td>
							Kondisi BMD</td>
							<td>:</td>
							<td><select  class="form-control" name="permasalahan_kondisi_bmd" required>
								<option value="Baik" <?php if ($kib_a['permasalahan_kondisi_bmd']=="Baik") {
									echo "selected";
								}?> >Baik</option>
								<option value="Rusak" <?php if ($kib_a['permasalahan_kondisi_bmd']=="Rusak") {
									echo "selected";
								}?> >Rusak</option>
								<option value="Rusak Berat" <?php if ($kib_a['permasalahan_kondisi_bmd']=="Rusak Berat") {
									echo "selected";
								}?> >Rusak Berat</option>
								</select>
                          </td>
                        </tr>
                      </tbody>
                    </table>
					<br>
					<center>
						<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp; Simpan Data</button>
					</center>
					<br>
                  </div>
                </div>
              </div>
            </div>
			<div class="col-md-4">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Gambar</h4>
                  <p class="card-category">KIB A (Tanah)</p>
                </div>
                <div class="card-body">
                  <div class="row">
				   <div class="col-sm-6 col-lg-5">
					<img style="border-radius:1% max-width: 293px; max-height: 250px; min-height: 250px; min-width: 293px" width='293' height='250' src='images/<?= $kib_a['gambar']; ?>' id="output_image" />
					<input type="file" name="gambar" accept="images/<?= $kib_a['gambar']; ?>" onchange="preview_image(event)" class="form-control" required>
                  <br>
                  </div>
                  </form>
                </div>
              </div>
            </div>
           </div>
         </div>