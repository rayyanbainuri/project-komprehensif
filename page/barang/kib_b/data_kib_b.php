<style type="text/css">
  #myTable_info{
    color :black;
  }
  #myTable_paginate a{
    color: black;
  }
</style>
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-header-primary">
				<!-- <a href="index.php?page=form_jenis_barang&kib=b" class="btn btn-success" style="float:right"><i class="fa fa-plus-square"></i>&nbsp;&nbsp;&nbsp; Tambah Jenis Barang</a> -->
				<a href="index.php?page=form_input_kib_b" class="btn btn-success" style="float:right"><i class="fa fa-plus-square"></i>&nbsp;&nbsp;&nbsp; Tambah Data</a>
          <h4 class="card-title"><strong>Daftar Barang</strong></h4>
            <p class="card-category">KIB B (Peralatan dan Mesin)</p>
      </div>
        <div class="card-body">
          <div class="row">
          	<div class="col-sm-6 ">
								<label for="exampleInput1" class="bmd-label-floating">Pilih Jenis Barang</label>
          <form action="index.php?page=data_kib_b" method="post">
								<select  class="form-control" name="id_jenis" onchange="this.form.submit()" required>
                <option selected disabled>- Pilih jenis barang -</option>
                  <?php foreach ($jenis_barang->getData('b') as $val ): ?>
                    <option value="<?php echo $val['id_jenis'] ?>"><?php echo $val['jenis_barang'] ?></option>
                  <?php endforeach ?>
                </select>
          </form>
							</div>
          </div>
           <?php if(isset($_POST['id_jenis'])){?>
          <div class="table-responsive">
            <table class="table" id="myTable">
              <thead class=" text-primary">
                 <th>
                   <center>Register</center>
                 </th>
                 <th>
                   Jenis Barang
                 </th>
                <th>
                  <center>Perolehan</center>
                </th>
                <th>
                  <center>Usia</center>
                </th>
                 <th>
                   <center>Kondisi</center>
                 </th>
                 <th>
                   <center>Action</center>
                 </th>
                 <th>
                   <center>Action</center>
                 </th>
              </thead>
              <tbody>
                <?php
                $tgl_proleh = 0;
                 foreach ($barang->getDataNew_Kib_B_IRE($_POST['id_jenis']) as $data_kib_b) { 
                $tgl_proleh = $data_kib_b['tgl_perolehan'];
                  ?>
                  <tr style="background-color: #dadada">
                    <td width="10px" style="color: #000000">
                      <center><?= $data_kib_b['register_kib_b'] ?></center>
                    </td>
                    <td width="10px" style="color: #000000">
                      <?php  $jn=$jenis_barang->getDetail($data_kib_b['id_jenis']);
                                      echo $jn['jenis_barang']; ?>
                    </td>
                    <td width="10px" style="color: #000000">
                       <center><?= $data_kib_b['tgl_perolehan'] ?></center>
                    </td>
                    <td width="10px" style="color: #000000">
                       <center><?php 
                                      $rubah = date('Y',strtotime($tgl_proleh));
                                      $thn_skrg = date('Y');
                                      $usia = $thn_skrg-$rubah;
                                      echo $usia;
                                      ?> Tahun</center>
                    </td>
                    <td width="10px">
                      <center><a class="btn btn-<?php if ($data_kib_b['permasalahan_kondisi_bmd']=='Baik'){echo "success";} else if ($data_kib_b['permasalahan_kondisi_bmd']=='Rusak'){echo "danger";} if ($data_kib_b['permasalahan_kondisi_bmd']=='Rusak Berat'){echo "warning";}?>" style="width:130px"><?= $data_kib_b['permasalahan_kondisi_bmd'] ?></a></center>
                    </td>
						        
                    <td width="10px">
						          <center><a href="index.php?page=detail_kib_b&register_kib_b=<?=$data_kib_b['register_kib_b']?>" class="btn btn-info"><i class="material-icons">visibility</i> Detail </a></center>
						        </td>
						  
                    <td width="10px">
						          <center>
                        <a href="index.php?page=form_ubah_kib_b&register_kib_b=<?=$data_kib_b['register_kib_b']?>" rel="tooltip" title=""data-original-title="Ubah Data"><i class="fa fa-edit fa-lg fa-12"></i></a>
                        <a href="controller/kib_b/delete_kib_b.php?&register_kib_b=<?=$data_kib_b['register_kib_b']?>" onclick="if (!confirm(&quot;Yakin untuk Menghapus Data Ini ?&quot;)) {return false;}" rel="tooltip" title=""data-original-title="Hapus Data"><i class="fa fa-trash-o fa-lg fa-12"></i></a>
                      </center>
						        </td>
                  </tr>
              <?php } ?> 
            </tbody>
						</table>
        </div>
              <?php } ?> 
      </div>
    </div>
  </div>
</div>