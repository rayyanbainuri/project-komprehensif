<div class="row">
	<div class="col-md-8">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title"><strong>Form Input Data</strong></h4>
                <p class="card-category">KIB B (Peralatan & Mesin)</p>
            </div>
            <form action="controller/kib_b/create_kib_b.php" method="Post" enctype="multipart/form-data">
			<div class="card-body">
				<div class="row">
					<div class="col-sm-12 col-lg-12">
						<div class="form-group">
							<label for="exampleInput1" class="label">Jenis Barang</label>
							<select  class="form-control" id="id_jenis" onchange="tampilkan()" name="id_jenis" required>
									<?php foreach ($jenis_barang->getData('b') as $val ): ?>
										<option value="<?php echo $val['id_jenis'] ?>"><?php echo $val['jenis_barang'] ?></option>
									<?php endforeach ?>
								</select>
							<span class="bmd-help"></span>
						</div>
						<div id="tampil"></div>
					
                  </div>
                </div>
              </div>
            </div>
           </div>
			<div class="col-md-4">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title"><strong>Gambar</strong></h4>
                  <p class="card-category">KIB B (Peralatan & Mesin)</p>
                </div>
                <div class="card-body">
                  <div class="row">
                   <div class="col-sm-6 col-lg-5">
				   	<img style="border-radius:1%" width='300' height='300' src='assets/img/no-image.png' id="output_image" />
                	<input type="file" name="gambar" accept="image/x-png,image/gif,image/jpeg" onchange="preview_image(event)" class="form-control" required>
                <br>
                </div>
            </div>
        </div>
    </div>
</div>

 <script type='text/javascript'>
 	function tampilkan(){
var id_jenis=document.getElementById("id_jenis").value;

  if (id_jenis==9||id_jenis==57||id_jenis==52)
    {
    	  document.getElementById("tampil").innerHTML='<div class="form-group">							<label for="exampleInput1" class="label">Komponen</label>							<input type="text" class="form-control" name="komponen">							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Ukuran</label>							<input type="text" class="form-control" name="ukuran">							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Satuan</label>							<select class="form-control" name="satuan"><option value="CC">CC</option><option value="HP">Horsepower</option></select>							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Tanggal Perolehan</label>							<input type="date" class="form-control" name="tgl_perolehan" required>							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Bahan</label>							<select class="form-control" name="bahan"><option value="Besi">Besi</option><option value="Baja">Baja</option><option value="Alumunium">Alumunium</option><option value="Plastik">Plastik</option></select>							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Merk</label>							<input type="text" class="form-control" name="merk">							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Type</label>							<input type="text" class="form-control" name="type">							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Tanggal BPKB</label>							<input type="date" class="form-control" name="tgl_bpkb">							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Nomor BPKB</label>							<input type="text" class="form-control" name="nomor_bpkb">							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">No. Chasis / No. Rangka Mesin</label>							<input type="text" class="form-control" name="no_chasis">							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">No. Mesin / No. Pabrik</label>							<input type="text" class="form-control" name="no_mesin">							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">No. Polisi</label>							<input type="text" class="form-control" name="no_polisi">							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Asal-usul Perolehan / Anggaran</label>							<select class="form-control" name="asal_usul_perolehan"><option value="Hibah">Hibah</option><option value="Pembelian">Pembelian</option></select>							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Harga (Rp.)</label>							<input type="text" class="form-control" name="harga" required>							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Keterangan</label>							<input type="text" class="form-control" name="keterangan">							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Kondisi BMD</label>							<select  class="form-control" name="permasalahan_kondisi_bmd" required>								<option value="Baik">Baik</option>								<option value="Rusak">Rusak</option>								<option value="Rusak Berat">Rusak Berat</option>							</select>							<span class="bmd-help"></span>						</div>						<br>						<center>						<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp; Tambah Data</button>						</center>						<br>';
    }else{
    		 document.getElementById("tampil").innerHTML='<div class="form-group">							<label for="exampleInput1" class="label">Komponen</label>							<input type="text" class="form-control" name="komponen">							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Ukuran</label>							<input type="text" class="form-control" name="ukuran">							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Satuan</label>							<select class="form-control" name="satuan"><option value="Unit">Unit</option><option value="Buah">Buah</option><option value="m">m</option><option value="cm">cm</option></select>							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Tanggal Perolehan</label>							<input type="date" class="form-control" name="tgl_perolehan" required>							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Bahan</label>							<select class="form-control" name="bahan"><option value="Besi">Besi</option><option value="Baja">Baja</option><option value="Plastik">Plastik</option><option value="Alumunium">Alumunium</option><option value="Kain">Kain</option><option value="Kayu">Kayu</option><option value="Stainless">Stainless Steel</option><option value="Kaca">Kaca</option></select>							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Merk</label>							<input type="text" class="form-control" name="merk">							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Type</label>							<input type="text" class="form-control" name="type">							<span class="bmd-help"></span>						</div>												<div class="form-group">							<label for="exampleInput1" class="label">Asal-usul Perolehan / Anggaran</label>							<select  class="form-control" name="asal_usul_perolehan" required>								<option value="Hibah">Hibah</option>								<option value="Pembelian">Pembelian</option></select>							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Harga (Rp.)</label>							<input type="text" class="form-control" name="harga" required>							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Keterangan</label>							<input type="text" class="form-control" name="keterangan">							<span class="bmd-help"></span>						</div>						<div class="form-group">							<label for="exampleInput1" class="label">Kondisi BMD</label>							<select  class="form-control" name="permasalahan_kondisi_bmd" required>								<option value="Baik">Baik</option>								<option value="Rusak">Rusak</option>								<option value="Rusak Berat">Rusak Berat</option>							</select>							<span class="bmd-help"></span>						</div>						<br>						<center>						<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp; Tambah Data</button>						</center>						<br>';
    }

}
function preview_image(event)
{
 var reader = new FileReader();
 reader.onload = function()
 {
  var output = document.getElementById('output_image');
  output.src = reader.result;
 }
 reader.readAsDataURL(event.target.files[0]);
}</script>

						