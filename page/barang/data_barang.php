
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Daftar Barang</h4>
                  <p class="card-category">Daftar Barang terbaru</p>
				  <a class="btn btn-success">Tambah Data</a>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th>
                          ID
                        </th>
                        <th>
                          No Regis
                        </th>
                        <th>
                          Nama Barang
                        </th>
                        <th>
                          Kondisi
                        </th>
						<th>
                          Action
                        </th>
                      </thead>
                      <tbody>
                        <tr>
                          <td  width="10px">
                            1
                          </td>
                          <td width="100px">
                            09092018
                          </td>
                          <td>
                            Niger
                          </td>
                          <td width="50px">
                            <a class="btn btn-success">Layak</a>
                          </td>
						  <td width="20px">
						  <a href="index.php?page=detail_barang" class="btn btn-info"><i class="material-icons">visibility</i> Detail </a>
						  </td>
                        </tr>
                        <tr>
                          <td>
                            2
                          </td>
                          <td>
                            09092018
                          </td>
                          <td>
                            Curaçao
                          </td>
                          <td>
                            <a class="btn btn-warning">Perbaikan</a>
                          </td>
						  <td width="20px">
						  <a href="index.php?page=detail_barang" class="btn btn-info"><i class="material-icons">visibility</i> Detail </a>
						  </td>
                        </tr>
                        <tr>
                          <td>
                            3
                          </td>
                          <td>
                            09092018
                          </td>
                          <td>
                            Netherlands
                          </td>
                          <td>
                           <a class="btn btn-danger">Tidak Layak</a>
                          </td>
						  <td width="20px">
						  <a href="index.php?page=detail_barang" class="btn btn-info"><i class="material-icons">visibility</i> Detail </a>
						  </td>
                          
                        </tr>
                        <tr>
                          <td>
                            4
                          </td>
                          <td>
                            09092018
                          </td>
                          <td>
                            Korea, South
                          </td>
                          <td>
                            <a class="btn btn-success">Layak</a>
                          </td>
						  <td width="20px">
						  <a href="index.php?page=detail_barang"class="btn btn-info"><i class="material-icons">visibility</i> Detail </a>
						  </td>
                          
                        </tr>
                        <tr>
                          <td>
                            5
                          </td>
                          <td>
                           09092018
                          </td>
                          <td>
                            Malawi
                          </td>
                          <td>
                            <a class="btn btn-warning">Perbaikan</a>
                          </td>
						  <td width="20px">
						  <a href="index.php?page=detail_barang"class="btn btn-info"><i class="material-icons">visibility</i> Detail </a>
						  </td>
                          
                        </tr>
                        <tr>
                          <td>
                            6
                          </td>
                          <td>
                           09092018
                          </td>
                          <td>
                            Chile
                          </td>
                          <td>
                            <a class="btn btn-danger">Tidak Layak</a>
                          </td>
						  <td width="20px">
						  <a href="index.php?page=detail_barang" class="btn btn-info"><i class="material-icons">visibility</i> Detail </a>
						  </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            </div>
        