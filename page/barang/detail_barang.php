
          <div class="row">
            <div class="col-md-6">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Detail Barang</h4>
                  <p class="card-category">Detail Barang</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <td width="150px">
						ID Barang</td>
						<td>:</td>
						<td>1</td>
	
                      </thead>
                      <tbody>
                        <tr>
							<td>
							NO Regis</td>
							<td>:</td>
							<td>20122018</td>
                        </tr>
                       
                          <tr>
							<td>
							Tanggal Masuk</td>
							<td>:</td>
							<td>12-08-2018</td>
                        </tr>
                        
                        
                          <tr>
							<td>
							Nama Barang</td>
							<td>:</td>
							<td>Kursi</td>
                        </tr>
                        
                        
                          <tr>
							<td>
							Jenis</td>
							<td>:</td>
							<td>Kayu</td>
                        </tr>
                          
                        
                          <tr>
							<td>
							Merk</td>
							<td>:</td>
							<td>Adidas</td>
                        </tr>
                          
                        
                        <tr>
							<td>
							Warna</td>
							<td>:</td>
							<td>Hitam</td>
                        </tr>
                        <tr>
							<td>
							Jumlah</td>
							<td>:</td>
							<td>45</td>
                        </tr>
						
                        <tr>
							<td>
							No Surat</td>
							<td>:</td>
							<td>09</td>
                        </tr>
						
                        <tr>
							<td>
							Kondisi</td>
							<td>:</td>
							<td><a class="btn btn-success">Layak</a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
			<div class="col-md-6">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Gambar</h4>
                  <p class="card-category">Gambar Barang</p>
                </div>
                <div class="card-body">
                  <div class="row">
                   <div class="col-sm-6 col-lg-5">
				   <img style="border-radius:1%" width='470' height='350' src='assets/img/no-image.png' id="output_image" />
                
                 <br>
                  </div>
                </div>
              </div>
            </div>
           </div>
         </div>
        