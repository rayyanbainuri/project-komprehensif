<style type="text/css">
  #myTable_info{
    color :black;
  }
  #myTable_paginate a{
    color: black;
  }
</style>

<?php if($_GET['kib']=='a'){?>
<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header card-header-primary">
				<h4 class="card-title"><strong>Form jenis Barang</strong></h4>
            <p class="card-category">KIB A (Tanah)</p>
      </div>
        <div class="card-body">
          <div class="table-responsive">
            <?php if(isset($_GET['action'])=="update"){?>
            <form action="controller/jenis/update_jns.php?kib=a" method="post">
            	
            	<div class="form-group">
								<label for="exampleInput1" class="bmd-label-floating"></label>
								<input type="hidden" class="form-control" name="id_jenis" value="<?= $_GET['id_jenis']?>">
								<span class="bmd-help"></span>
							</div>
							<div class="form-group">
								<label for="exampleInput1" class="bmd-label-floating">Jenis Barang</label>
								<input type="text" class="form-control" name="jenis_barang" value="<?php  $jn=$jenis_barang->getDetail($_GET['id_jenis']);
                                      echo $jn['jenis_barang']; ?>" required>
								<span class="bmd-help"></span>
							</div>
           <center>
								<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp; Ubah Data</button>
							</center>
            </form>
            <?php }else{?>
            <form action="controller/jenis/create_jns.php?kib=a" method="post">
            <div class="form-group">
								<label for="exampleInput1" class="bmd-label-floating"></label>
								<input type="hidden" class="form-control" name="kib" value="a">
								<span class="bmd-help"></span>
							</div>
							<div class="form-group">
								<label for="exampleInput1" class="bmd-label-floating">Jenis Barang</label>
								<input type="text" class="form-control" name="jenis_barang" required>
								<span class="bmd-help"></span>
							</div>
           <center>
								<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp; Tambah Data</button>
							</center>
            </form>
            <?php }?>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-6">
		<div class="card">
			<div class="card-header card-header-primary">
				<h4 class="card-title"><strong>Daftar Jenis Barang</strong></h4>
            <p class="card-category">KIB A (Tanah)</p>
      </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table" id="myTable">
              <thead class="text-primary">
                <th>
                  <center>No</center>
                </th>
                <th>
                  Jenis Barang
                </th>
                 <th>
                  <center>Action</center>
                </th>
              </thead>
              <tbody >
                <?php $no=1; foreach ($jenis_barang->getData('a') as $data_kib_a) : ?>
                  <tr style="background-color: #dadada">
                    <td width="10px" style="color: #000000">
                       <center><?= $no++ ?></center>
                    </td>
                    <td width="10px" style="color: #000000">
                       <?php  $jn=$jenis_barang->getDetail($data_kib_a['id_jenis']);
                                      echo $jn['jenis_barang']; ?>
                    </td>
                    <td width="20px">
                      <center>
                      <a href="index.php?page=form_jenis_barang&id_jenis=<?=$data_kib_a['id_jenis']?>&kib=a&action=update" rel="tooltip" title=""data-original-title="Ubah Data"><i class="fa fa-edit fa-lg fa-12"></i></a>
                      <a href="controller/jenis/delete_jns.php?&id_jenis=<?=$data_kib_a['id_jenis']?>&kib=a" onclick="if (!confirm(&quot;Yakin untuk Menghapus Data Ini ?&quot;)) {return false;}" rel="tooltip" title=""data-original-title="Hapus Data"><i class="fa fa-trash-o fa-lg fa-12"></i></a>
                      </center>
                    </td>
                  </tr>
               <?php endforeach?> 
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } if($_GET['kib']=='b'){?>
<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-header card-header-primary">
				<h4 class="card-title"><strong>Form jenis Barang</strong></h4>
            <p class="card-category">KIB B (Peralatan dan Mesin)</p>
      </div>
        <div class="card-body">
          <div class="table-responsive">
            <?php if(isset($_GET['action'])){?>
            <form action="controller/jenis/update_jns.php?kib=b" method="post">
            	
            	<div class="form-group">
								<label for="exampleInput1" class="bmd-label-floating"></label>
								<input type="hidden" class="form-control" name="id_jenis" value="<?= $_GET['id_jenis']?>">
								<span class="bmd-help"></span>
							</div>
							<div class="form-group">
								<label for="exampleInput1" class="bmd-label-floating">Jenis Barang</label>
								<input type="text" class="form-control" name="jenis_barang" value="<?php  $jn=$jenis_barang->getDetail($_GET['id_jenis']);
                                      echo $jn['jenis_barang']; ?>" required>
								<span class="bmd-help"></span>
							</div>
           <center>
								<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp; Ubah Data</button>
							</center>
            </form>
            <?php }else{?>
            <form action="controller/jenis/create_jns.php?kib=b" method="post">
            <div class="form-group">
								<label for="exampleInput1" class="bmd-label-floating"></label>
								<input type="hidden" class="form-control" name="kib" value="b">
								<span class="bmd-help"></span>
							</div>
							<div class="form-group">
								<label for="exampleInput1" class="bmd-label-floating">Jenis Barang</label>
								<input type="text" class="form-control" name="jenis_barang" required>
								<span class="bmd-help"></span>
							</div>
           <center>
								<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp; Tambah Data</button>
							</center>
            </form>
            <?php }?>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-6">
		<div class="card">
			<div class="card-header card-header-primary">
				<h4 class="card-title"><strong>Daftar Jenis Barang</strong></h4>
            <p class="card-category">KIB B (Peralatan dan Mesin)</p>
      </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table" id="myTable">
              <thead class="text-primary">
                <th>
                  <center>No</center>
                </th>
                <th>
                  Jenis Barang
                </th>
                 <th>
                  <center>Action</center>
                </th>
              </thead>
              <tbody >
                <?php $no=1; foreach ($jenis_barang->getData('b') as $data_kib_b) : ?>
                  <tr style="background-color: #dadada">
                    <td width="10px" style="color: #000000">
                       <center><?= $no++ ?></center>
                    </td>
                    <td width="10px" style="color: #000000">
                       <?php  $jn=$jenis_barang->getDetail($data_kib_b['id_jenis']);
                                      echo $jn['jenis_barang']; ?>
                    </td>
                    <td width="20px">
                      <center>
                      <a href="index.php?page=form_jenis_barang&id_jenis=<?=$data_kib_b['id_jenis']?>&kib=b&action=update" rel="tooltip" title=""data-original-title="Ubah Data"><i class="fa fa-edit fa-lg fa-12"></i></a>
                      <a href="controller/jenis/delete_jns.php?&id_jenis=<?=$data_kib_b['id_jenis']?>&kib=b" onclick="if (!confirm(&quot;Yakin untuk Menghapus Data Ini ?&quot;)) {return false;}" rel="tooltip" title=""data-original-title="Hapus Data"><i class="fa fa-trash-o fa-lg fa-12"></i></a>
                      </center>
                    </td>
                  </tr>
               <?php endforeach?> 
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php }?>