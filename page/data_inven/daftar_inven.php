     <?php 
        $bulan=array(
        "1"=>"Januari",
        "2"=>"Februari",
        "3"=>"Maret",
        "4"=>"April",
        "5"=>"Mei",
        "6"=>"Juni",
        "7"=>"Juli",
        "8"=>"Agustus",
        "9"=>"September",
        "10"=>"Oktober",
        "11"=>"November",
        "12"=>"Desember");
        ?> 

<div class="row">
	<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
    <div class="card card-stats">
      <div class="card-header card-header-success card-header-icon">
        <div class="card-icon">
          <i class="fa fa-building"></i>
        </div>
          <h1 class="card-title"><strong>KIB A</strong></h1>
				  <p class="card-category">(Tanah)</p>
      </div>
        <div class="card-footer">
          <div class="stats">
            <form action="page/print/print_kib_a.php" target="_blank" method="Post">
              <label >Pilih Jenis Barang</label>
            <select  class="form-control" name="id_jenis" required>
                  <option value="All">All</option>
                  <?php foreach ($jenis_barang->getData('a') as $val ): ?>
                    <option value="<?php echo $val['id_jenis'] ?>"><?php echo $val['jenis_barang'] ?></option>
                  <?php endforeach ?>
                </select>
                <label >Pilih Bulan</label >
            <select class="form-control" name="bulan" required>
              <option>- Pilih Bulan -</option>
              <?php foreach ($barang->getBulan_kib_a() as $key): 
                $va =$key['bulan']."-".$key['tahun'];
                ?>
              <option value="<?= $key['tahun']?>-<?= $key['bulan']?>" <?php if(isset($_POST['bulan'])){ if($_POST['bulan']==$va&&$_POST['kib']=="kib_a"){ echo "selected"; } }?>><?= $bulan[$key['bulan']]?> - <?= $key['tahun']?></option>
              <?php endforeach ?>  
            </select>
            <br>
            <button type="submit" class="btn btn-success"><strong><i class="fa fa-print"></i>&nbsp;&nbsp;Print</strong></button>
            <button type="submit" name="excel" class="btn btn-success"><strong><i class="fa fa-file-excel-o"> </i>&nbsp;&nbsp;Export to EXCEL</strong></button>
            </form>
          </div>
        </div>
    </div>
  </div>
			
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon">
                    <i class="fa fa-desktop"></i>
                  </div>
                  <h1 class="card-title"><strong>KIB B</strong></h1>
				  <p class="card-category">(Peralatan & Mesin)</p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <form action="page/print/print_kib_b.php" target="_blank" method="Post">
                      <label >Pilih Jenis Barang</label>
                    <select  class="form-control" name="id_jenis" required>
                      <option value="All">All</option>
                  <?php foreach ($jenis_barang->getData('b') as $val ): ?>
                    <option value="<?php echo $val['id_jenis'] ?>"><?php echo $val['jenis_barang'] ?></option>
                  <?php endforeach ?>
                </select>
                <label >Pilih Bulan</label >
                 <select class="form-control" name="bulan" required>
                    <option>- Pilih Bulan -</option>
                    <?php foreach ($barang->getBulan_kib_b() as $key): 
                      $va =$key['bulan']."-".$key['tahun'];
                      ?>
                    <option value="<?= $key['tahun']?>-<?= $key['bulan']?>" <?php if(isset($_POST['bulan'])){ if($_POST['bulan']==$va&&$_POST['kib']=="kib_b"){ echo "selected"; } }?>><?= $bulan[$key['bulan']]?> - <?= $key['tahun']?></option>
                    <?php endforeach ?>  
                  </select>
                    <br>
                    <button type="submit" class="btn btn-success"><strong><i class="fa fa-print"></i>&nbsp;&nbsp;Print</strong></button>
                    <button type="submit" name="excel" class="btn btn-success"><strong><i class="fa fa-file-excel-o "> </i>&nbsp;&nbsp;Export to EXCEL</strong></button>
                    </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>